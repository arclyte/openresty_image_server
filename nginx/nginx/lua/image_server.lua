local http = require('socket.http')

local height, width, image_path =
  ngx.var.arg_height, ngx.var.arg_width, ngx.var.arg_uri

local function return_404(msg)
	ngx.status = ngx.HTTP_NOT_FOUND
	ngx.header["Content-type"] = "text/html"
	-- ngx.say(msg or "not found")
	ngx.say(msg or image_path)
	ngx.exit(0)
end


ngx.say(height)
ngx.say(width)
ngx.say(image_path)
-- local file = io.open(image_path);

-- if not file then
-- 	return_404()
-- end

-- file:close()

-- ngx.exec(ngx.var.request_uri)