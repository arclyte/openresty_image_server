--[[
	Image Manipulation Library

	The purpose of this script is to process image files uploaded to Nginx using the nginx-upload-module.
	It receives posted data from the upload module and parses the form data, then identifies to file type,
	running any manipulations or optmiziations required, and then uploads the optimized image to S3 for storage.
--]]

ngx.req.read_body()
--
local uuid4 = require("uuid4")
local cjson = require("cjson")
--
-- from: Lua String Magic (http://coronalabs.com/blog/2013/04/16/lua-string-magic/)
function string:split( inSplitPattern, outResults )
   if not outResults then
      outResults = { }
   end
   local theStart = 1
   local theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
   while theSplitStart do
      table.insert( outResults, string.sub( self, theStart, theSplitStart-1 ) )
      theStart = theSplitEnd + 1
      theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
   end
   table.insert( outResults, string.sub( self, theStart ) )
   return outResults
end
--
local function findBoundary()
	mb_start, mp_end = string.find(ngx.var.http_content_type, "multipart/form-data; ", 1, true)

	if mb_start ~= nil then
		mime_boundary = string.match(ngx.var.http_content_type, "boundary=(.*)", mp_end)
	else
		-- todo: error handling
		ngx.say("Bad Content Type")
	end

	return mime_boundary
end
--
local function processPost(mime_form)
	local form = {}
	
	for i = 1, #mime_form do
		local line = string.match(mime_form[i], "Content%-Disposition:form%-data;name=\"")

		content_start, content_end = string.find(mime_form[i], "Content%-Disposition:form%-data;name=\"")

		if content_start ~= nil then
			content = string.sub(mime_form[i], content_end)

			if content ~= nil and content ~= "" then
				form_field, field_value = string.match(content, "\"(.*)\"(.*)")

				if form_field ~= nil then
					form_field = string.gsub(form_field, '%.', '_')
					form[form_field] = field_value
				end
			end
		end
	end
		
	return form
end
--
local function validateForm(form)
	if form.file1_name ~= nil and form.file1_name ~= "" then
		if  0 < tonumber(form.file1_size) then
			return true
		end
	end

	return false
end
--
local function exec(command)
	local handle = io.popen(command)
	local result = handle:read('*a')
	handle:close()

	return result
end
--
-- local function ffprobe(path)
-- 	local ffprobe_command = "/home/jamesalday/bin/ffprobe -print_format json -loglevel quiet -show_format -show_streams "
-- 	local result = exec(ffprobe_command .. path)
-- 	local json = cjson.decode(result)
	
-- 	if json ~= nil then
-- 		-- parse format section if it is set
-- 		if json["format"] ~= nil then
-- 			if json["format"]["filename"] ~= nil then
-- 				ngx.print(json["format"]["filename"])
-- 				ngx.print("\n")
-- 			end
-- 		end

-- 		-- parse stream section if it is set
-- 		if json["streams"] ~= null then
-- 			ngx.print(json["streams"][1]["codec_name"])
-- 		end
-- 	else
-- 		ngx.print('probe was nil', "\n")
-- 	end

-- 	return result
-- end
--
-- Use imagemagick's 'identify' to get image format
local function imagick(path)
	local imagick_command = "identify -format '%m' " -- image type
	local result = exec(imagick_command .. path)

	if result ~= nil then
		result, count = string.gsub(result, "GIF", "GIF")

		if count > 0 then
			result = "GIF"
			
			if count > 1 then
				result = "GIF-ANIMATED"
			end
		end
	end

	return result
end
--
local function identify(path)
	-- result = ffprobe(path)

	-- if result['format']['filename'] ~= nil then
	-- 	return result
	-- end

	result = imagick(path)

	return result
end
--
local function copy(path, ext)
	copy = "cp " .. path .. " " .. path .. ext
end
--
local function processJPG(path)
	exec("jhead -purejpg " .. path)
end
--
local function processPNG(path)
	exec("/usr/bin/optipng -o5 " .. path)
end
--
local function processGIF(path)
	-- TODO: change extension to .png
	exec("convert " .. path)
end
--
local function processGIFAnimated(path)
	-- todo
	exec("gifsicle -O3 " .. path)
end
--
local function process(path)
	local uuid = uuid4.getUUID()

	if file_format ~= nil then
		if file_format == "JPEG" or file_format == "JPG" then
			processJPG(path)
		elseif file_format == "PNG" then
			processPNG(path)
		elseif file_format == "GIF" then
			processGIF(path)
		elseif file_format == "GIF-ANIMATED" then
			processGIFAnimated(path)
		else
			return false
		end
	end
end
--
local mime_boundary = findBoundary()

local mime_form = ngx.var.request_body:gsub("%s", "")
mime_form = mime_form:split(mime_boundary)

local form = processPost(mime_form)

if validateForm(form) then
	local path = form.file1_path

	local file_format = identify(path)

	process(file_format, path)
else
	ngx.print('bad form')
end