#!/bin/bash

INPUT_FILE=$1
FILENAME=${INPUT_FILE%%.*}

# Encode MP4
ffmpeg -i $INPUT_FILE -loglevel warning -vcodec libx264 -pix_fmt yuv420p -profile:v baseline -preset slower -crf 18 -vf "scale=trunc(in_w/2)*2:trunc(in_h/2)*2" $FILENAME.mp4

# Encode WebM
ffmpeg -i $INPUT_FILE -loglevel warning -c:v libvpx -an -pix_fmt yuv420p -quality good -b:v 2M -crf 5 $FILENAME.webm

# Encode ogv
ffmpeg -i $INPUT_FILE -loglevel warning -q 5 -pix_fmt yuv420p -an -vcodec libtheora $FILENAME.ogv

# Output Poster Image
ffmpeg -i $INPUT_FILE -loglevel warning -vframes 1 -map 0:v:0 $FILENAME.poster.png

# Output Thumbnail Image
#ffmpeg -i $INPUT_FILE -loglevel warning -vframes 1 -map 0:v:0 -vf "scale=100:100" $FILENAME.thumb.png